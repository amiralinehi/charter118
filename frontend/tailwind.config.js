/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "main-color": "#A02777",
        "header-menu-color": "#F2F2F2",
        "button-header-menu-color": "#FEAD11",
        "main-color-darker": "#5C083F",
        "card-hover": "#E7BAD8",
        "modal-hover": "#F2F3F5",
      },
      fontFamily: {
        "vazir-matn": ["Vazir-Matn", "sans-serif"],
        shabnam: ["Shabnam", "sans-serif"],
      },
    },
  },
  plugins: [],
};
