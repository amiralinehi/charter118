import AboutPic from "../../media/about/about.jpg";

// background picture
const About = () => {
  const headerStyle = {
    backgroundImage: `url(${AboutPic})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    height: "300px", // adjust the height as needed
    // Add other styling properties as needed
  };

  return (
    <>
      <section
        className="container mt-4 gap-1 py-1 px-2 rounded-md shadow-md flex justify-center"
        style={headerStyle}
      >
        <div className="flex justify-between items-center">
          <section dir="rtl" id="about-text">
            <h2 className="text-right text-white text-sm">اطلاعیه های سایت</h2>
            {/* make yellow bullets */}
            <ul className=" text-button-header-menu-color list-disc text-sm">
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  سامانه ثبت شکایات
                </div>
              </li>
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  فروش واکسن کرونا در چه کشورهایی انجام میشود
                </div>
              </li>
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  روش تهیه بلیط هواپیما
                </div>
              </li>
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  ترمینال ایرلاین داخلی
                </div>
              </li>
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  لیست شرکت های هوایی ایران
                </div>
              </li>
              <li>
                <div className="text-white overflow-hidden overflow-ellipsis cursor-pointer">
                  قوانین بلیط هواپیما چارتر
                </div>
              </li>
            </ul>
          </section>
          <section
            dir="rtl"
            id="about-text"
            className="w-1/2 text-sm overflow-hidden overflow-ellipsis cursor-pointer"
          >
            <h2 className="text-right text-white text-sm">درباره ما</h2>
            <p className="text-right text-white">
              بلیط چارتر هواپیما خود را از ما بخواهید. چارتر 118 (charter118) با
              بیش یک دهه تجربه در فروش انواع بلیط هواپیما از جمله چارتری و
              سیستمی توانسته رضایت مشتریان خود را جلب کند. ما در مجموعه چارتر
              118 انواع بلیط لحظه آخری هواپیما و بلیط ارزان هواپیما را برای شما
              عزیزان آماده کرده ایم تا بتوانید با خیالی راحت و آسود نسبت به خرید
              بلیط خود اقدام کنید. در چارتر 118 میتوانید انواع هتل های داخلی و
              خارجی نیز مشاهده و رزرو کنید. همچنین انواع تورهای جذاب داخلی و
              خارجی را چارتر 118 با قیمت‌های ویژه برای شما آماده کرده است تا
              سفری دلنشین و خاطره انگیز همراه ما داشته باشید.
            </p>
          </section>
        </div>
      </section>
    </>
  );
};
export default About;
