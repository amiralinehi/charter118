import { BsTelegram } from "react-icons/bs";
import { LiaViber } from "react-icons/lia";

const WhatsappModal = () => {
  return (
    <div className="absolute right-1/2 w-96 h-auto">
      <div className="flex flex-col items-center bg-white w-full p-4 rounded-lg shadow-md">
        <div className="flex justify-between items-center bg-modal-hover w-full p-2 rounded-lg">
          <div>x</div>
          <div>به کمک نیاز دارید؟</div>
        </div>

        <div className="bg-white flex justify-between items-center  w-full p-2 rounded-lg hover:bg-modal-hover">
          <BsTelegram size={32} className="mr-2 text-blue-500"></BsTelegram>
          <div dir="rtl">
            <div className="font-bold text-sm text-gray-400">پشتیبانی</div>
            <div className="font-bold text-sm text-gray-600">Telegram</div>
          </div>
        </div>

        <div className="bg-white flex justify-between items-center  w-full p-2 rounded-lg hover:bg-modal-hover">
          <LiaViber size={32} className="mr-2 text-purple-500"></LiaViber>
          <div dir="rtl">
            <div className="font-bold text-sm text-gray-400">پشتیبانی</div>
            <div className="font-bold text-sm text-gray-600">تماس تلفنی</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WhatsappModal;
