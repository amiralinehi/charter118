import TopNav from "./navigation items/TopNav";
import BottonNav from "./navigation items/BottonNav";

const Navigation = () => {
  return (
    <>
      <TopNav></TopNav>
      <BottonNav></BottonNav>
    </>
  );
};

export default Navigation;
