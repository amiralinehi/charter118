import React from "react";

import { FaExchangeAlt } from "react-icons/fa";
// import { IoMdCalendar } from "react-icons/io";
import { BiSolidPlaneTakeOff } from "react-icons/bi";
import { BiSolidPlaneLand } from "react-icons/bi";

const HeaderMenu = () => {
  return (
    <>
      {/* menu */}
      <div className="flex gap-2 bg-header-menu-color h-44 w-fit relative px-1 justify-center m-auto items-center rounded-md shadow-md bottom-10 z-9">
        {/* top part of menu */}
        <div className="flex w-fit m-auto items-center bg-header-menu-color bg-transparent absolute right-0 -top-10 gap-2 pl-5 py-1">
          <div className="bg-header-menu-color bg-transparent w-fit m-auto h-12 flex justify-center items-center gap-2">
            <span className=" text-center w-24 h-auto bg-header-menu-color px-1 py-2 text-main-color bg-opacity-80 rounded-md hover:bg-header-menu-color cursor-pointer">
              پکیچ تور
            </span>
            <span className=" text-center w-24 h-auto bg-header-menu-color px-1 py-2 text-main-color bg-opacity-80 rounded-md hover:bg-header-menu-color cursor-pointer">
              حجوزات عرب
            </span>
            <span className="text-center w-24 h-auto bg-header-menu-color px-1 py-2 text-main-color bg-opacity-80 rounded-md hover:bg-header-menu-color cursor-pointer">
              پرواز خارجی
            </span>
            <span className="text-center w-24 h-auto bg-header-menu-color px-1 py-2 text-main-color bg-opacity-80 rounded-md hover:bg-header-menu-color cursor-pointer">
              هتل خارجی
            </span>
            <span className="text-center w-24 h-auto bg-header-menu-color px-1 py-2 text-main-color bg-opacity-80 rounded-md hover:bg-header-menu-color cursor-pointer">
              هتل داخلی
            </span>
          </div>
          <div>
            <span className="w-fit h-auto bg-green px-2 bg-header-menu-color text-main-color rounded-md py-2 text-xl text-center cursor-pointer">
              بلیط هواپیما
            </span>
          </div>
        </div>
        {/* calendar */}
        <div className="relative flex items-center text-gray-400 justify-end">
          {/* <IoMdCalendar className="w-5 h-5 absolute ml-3"></IoMdCalendar> */}
          <input
            type="date"
            name="search"
            placeholder="تاریخ رفت"
            autoComplete="off"
            aria-label="search"
            className="pr-7 pl-10 font-medium border-none text-black placeholder-gray-500 rounded-md ring-2 ring-gray-300 focus:ring-2 focus:ring-gray-500 text-right py-2"
          ></input>
        </div>
        {/* departure */}
        <div className="relative flex items-center text-gray-400 justify-end">
          <BiSolidPlaneTakeOff className="w-5 h-5 absolute ml-3"></BiSolidPlaneTakeOff>
          <input
            type="text"
            name="search"
            placeholder="مقصد"
            autoComplete="off"
            aria-label="search"
            className="pr-7 pl-10 font-medium border-none text-black placeholder-gray-500 rounded-md ring-2 ring-gray-300 focus:ring-2 focus:ring-gray-500 text-right py-2"
          ></input>
        </div>
        <div className="flex justify-end items-center">
          <FaExchangeAlt className=" text-main-color" size={18}></FaExchangeAlt>
        </div>
        {/* origin */}
        <div className="relative flex items-center text-gray-400 justify-end">
          <BiSolidPlaneLand className="w-5 h-5 absolute ml-3"></BiSolidPlaneLand>
          <input
            type="text"
            name="search"
            placeholder="مبدا"
            autoComplete="off"
            aria-label="search"
            className="pr-7 pl-10 font-medium border-none text-black placeholder-gray-500 rounded-md ring-2 ring-gray-300 focus:ring-2 focus:ring-gray-500 text-right py-2"
          ></input>
        </div>
        <div className="py-2 absolute left-1 -bottom-1 ">
          <p className=" font-semibold text-xs text-gray-500 mb-1">
            نمایش کمترین نرخ در صورت عدم ورود تاریخ
          </p>
          <input
            className="py-2 rounded-md text-center placeholder-main-color bg-button-header-menu-color font-medium text-lg"
            placeholder="جستجو"
          ></input>
        </div>
      </div>
    </>
  );
};

export default HeaderMenu;
