import backgroundImage from "../../../media/logo/header.jpg";
import HeaderMenu from "./HeaderMenu";

// parent of header menu
const Header = () => {
  const headerStyle = {
    backgroundImage: `url(${backgroundImage})`,
    backgroundSize: "cover",
    backgroundPosition: "center",
    height: "300px",
  };

  return (
    <>
      <header style={headerStyle}></header>
      <HeaderMenu></HeaderMenu>
    </>
  );
};

export default Header;
