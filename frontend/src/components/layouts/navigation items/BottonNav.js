import Logo from "../../../media/logo/lange_logo.png";
import { MdOutlineLocalHotel } from "react-icons/md";
import { RiHotelLine } from "react-icons/ri";
import { TfiHeadphoneAlt } from "react-icons/tfi";
import { SlPlane } from "react-icons/sl";
import { useState } from "react";
import ToursModal from "./modals/ToursModal";
import OtherModal from "./modals/OtherModal";

const BottonNav = () => {
  const [expandTours, setExpandTours] = useState(false);
  const [expandMore, setExpandMore] = useState(false);

  const onToursClick = () => {
    setExpandTours(!expandTours);
  };

  const onOtherClick = () => {
    setExpandMore(!expandMore);
  };

  return (
    <div className="flex items-center justify-evenly gap-1 mt-2">
      <ul className="flex gap-2">
        <span className="flex gap-3">
          <div onClick={onOtherClick} className="relative flex cursor-pointer">
            <h1 className="text-blue-300"> اپلیکیشن و ساین همکاری |</h1>
            {/* render modal when clicked */}
            {expandMore && (
              <OtherModal onClose={() => setExpandMore(false)}></OtherModal>
            )}
          </div>
          <div>
            <SlPlane fontSize="24" className="black"></SlPlane>
          </div>
        </span>
        <span className="flex gap-3">
          <div onClick={onToursClick} className="relative flex cursor-pointer">
            <h1 className="text-blue-300"> تور داخلی و خارجی |</h1>
            {/* render modal when clicked */}

            {expandTours && (
              <ToursModal onClose={() => setExpandTours(false)}></ToursModal>
            )}
          </div>
          <div>
            <RiHotelLine fontSize="24" className="black"></RiHotelLine>
          </div>
        </span>
        <span className="flex gap-3">
          <div>
            <h1 className="text-blue-300 cursor-pointer">
              جدول جریمه پرواز سیستمی داخلی |
            </h1>
          </div>
          <div>
            <TfiHeadphoneAlt fontSize="24" className="black"></TfiHeadphoneAlt>
          </div>
        </span>
        <span className="flex gap-3">
          <div>
            <h1 className="text-blue-300 cursor-pointer"> | هتل</h1>
          </div>
          <div>
            <MdOutlineLocalHotel
              fontSize="24"
              className="black"
            ></MdOutlineLocalHotel>
          </div>
        </span>
      </ul>
      <img src={Logo} alt="log-pic" className=" w-30" />
    </div>
  );
};

export default BottonNav;
