import { useState } from "react";
import { IoIosArrowDown } from "react-icons/io";
import ErtebatModal from "./modals/ErtebatModal";
import UserModal from "./modals/UserModal";

const TopNav = () => {
  const [isContactModalOpen, setIsContactModalOpen] = useState(false);
  // const [isBlogModalOpen, setIsBlogModalOpen] = useState(false);
  const [isUserMenuModalOpen, setIsUserMenuModalOpen] = useState(false);

  const handleContactClick = () => {
    setIsContactModalOpen(!isContactModalOpen);
  };

  // const handleBlogClick = () => {
  //   setIsBlogModalOpen(!isBlogModalOpen);
  // };

  const handleUserMenuClick = () => {
    setIsUserMenuModalOpen(!isUserMenuModalOpen);
  };

  return (
    <>
      <ul
        className="bg-main-color flex justify-between px-32 py-2 cursor-pointer"
        id="top-nav"
      >
        <li className="text-white">پشتیبانی: 07691006118</li>
        <ul style={{ display: "flex", gap: ".5rem", listStyle: "none" }}>
          <li
            style={{
              display: "flex",
              gap: "1rem",
              alignItems: "center",
              position: "relative",
            }}
          >
            <IoIosArrowDown fontSize="12" className="text-white" />
            <h1 className="text-white" onClick={handleContactClick}>
              ارتباط با ما
            </h1>
            {/* render modal when clicked */}
            {isContactModalOpen && (
              <ErtebatModal
                onClose={() => setIsContactModalOpen(false)}
              ></ErtebatModal>
            )}
          </li>
          <li
            style={{
              display: "flex",
              gap: "1rem",
              alignItems: "center",
              position: "relative",
            }}
          >
            <IoIosArrowDown fontSize="12" className="text-white" />
            <a
              href="#news-section"
              className="text-white"
              // onClick={handleBlogClick}
            >
              وبلاگ
            </a>
            {/* {isBlogModalOpen && (
              <div
                style={{
                  position: "absolute",
                  top: "100%",
                  left: 0,
                  backgroundColor: "#fff",
                  padding: "1rem",
                  border: "1px solid #ccc",
                  zIndex: 1,
                }}
              >
                <div className="flex flex-col">
                  <div className="w-full shadow-md">hello</div>
                </div>
              </div>
            )} */}
          </li>
          <li
            style={{
              display: "flex",
              gap: "1rem",
              alignItems: "center",
              position: "relative",
            }}
          >
            <IoIosArrowDown fontSize="12" className="text-white" />
            <h1 className="text-white" onClick={handleUserMenuClick}>
              منوی کاربری
            </h1>
            {/* render modal when clicked */}

            {isUserMenuModalOpen && (
              <UserModal
                onClose={() => setIsUserMenuModalOpen(false)}
              ></UserModal>
            )}
          </li>
        </ul>
      </ul>
    </>
  );
};

export default TopNav;
