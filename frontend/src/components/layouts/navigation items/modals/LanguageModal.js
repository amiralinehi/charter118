import { useRef, useEffect } from "react";

const LanguageModal = ({ onClose }) => {
  const modalRef = useRef();

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  return (
    <div
      ref={modalRef}
      className="absolute top-1/4 left-1/3 bg-gray-200 z-10 p-4 w-fit flex flex-col gap-3 shadow-md rounded-md"
    >
      <div className="flex flex-col justify-center items-center gap-1">
        <div className="text-gray-500 cursor-pointer">العربی</div>
        <div className="text-gray-500 cursor-pointer">english</div>
      </div>
    </div>
  );
};

export default LanguageModal;
