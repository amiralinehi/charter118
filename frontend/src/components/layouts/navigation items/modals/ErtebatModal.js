import { FaPhoneVolume } from "react-icons/fa6";
import { CiMail } from "react-icons/ci";
import { FaRegCreditCard } from "react-icons/fa6";
import { FaMobileScreenButton } from "react-icons/fa6";
import { SiAparat } from "react-icons/si";
import { FaFacebookF } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaTelegramPlane } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaYoutube } from "react-icons/fa";

import { useRef, useEffect } from "react";

const ErtebatModal = ({ onClose }) => {
  const modalRef = useRef();

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  return (
    <>
      <div
        ref={modalRef}
        className="absolute top-full left-1/2 bg-white z-10 p-4 w-fit flex flex-col gap-2 rounded-md"
      >
        <div className="flex flex-col gap-4 p-1" dir="rtl">
          <div className="text-gray-600 shadow-md rounded-md p-3 border border-gray-200 hover:bg-modal-hover transition delay-100 cursor-pointer">
            <span>
              <FaPhoneVolume size={18}></FaPhoneVolume>
            </span>
            <h1 className="text-right">07691006118</h1>
            <h1
              dir="rtl"
              className="text-right text-sm flex gap-1 whitespace-nowrap"
            >
              برای
              <h2
                dir="rtl"
                className="font-semibold whitespace-nowrap text-right text-sm"
              >
                تماس تلفنی
              </h2>
              کلیک کنید
            </h1>
          </div>

          <div className="text-gray-600 shadow-md rounded-md p-3 border border-gray-200 hover:bg-modal-hover transition delay-100 cursor-pointer">
            <FaMobileScreenButton size={18}></FaMobileScreenButton>
            <h1 className="text-right">09027006118</h1>
          </div>

          <div className="p-2 text-gray-600 shadow-md rounded-md hover:bg-modal-hover transition delay-100 cursor-pointer border border-gray-200">
            <CiMail size={18}></CiMail>
            <h1 className="text-right">charter118@gmail.com</h1>
          </div>
          <div className="p-2 shadow-md rounded-md border border-gray-200 hover:bg-modal-hover transition delay-100 text-gray-600">
            <FaRegCreditCard></FaRegCreditCard>
            <h1 className="text-right">5054-1670-0019-3467</h1>
          </div>
        </div>
        <div className="flex justify-end items-center text-blue-300 gap-3">
          <SiAparat size={18} className=" cursor-pointer"></SiAparat>
          <FaFacebookF size={18} className=" cursor-pointer"></FaFacebookF>
          <FaTwitter size={18} className=" cursor-pointer"></FaTwitter>
          <FaTelegramPlane
            size={18}
            className=" cursor-pointer"
          ></FaTelegramPlane>
          <FaInstagram size={18} className=" cursor-pointer"></FaInstagram>
          <FaYoutube size={18} className=" cursor-pointer"></FaYoutube>
        </div>
      </div>
    </>
  );
};

export default ErtebatModal;
