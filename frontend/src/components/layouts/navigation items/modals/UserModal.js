import { FaUserCircle } from "react-icons/fa";
import { IoLanguageSharp } from "react-icons/io5";
import { IoIosArrowDown } from "react-icons/io";
import { FaHouse } from "react-icons/fa6";
import { FaInfo } from "react-icons/fa";
import { GrCatalog } from "react-icons/gr";
import { GiInjustice } from "react-icons/gi";
import { ImTicket } from "react-icons/im";
import LanguageModal from "../modals/LanguageModal";

import { useRef, useEffect, useState } from "react";

const UserModal = ({ onClose }) => {
  const [expandRules, setExpandRules] = useState(false);
  const [expandlanguage, setExpandLanguage] = useState(false);

  const modalRef = useRef();

  const onExpandRules = () => {
    setExpandRules(!expandRules);
  };

  const onLanguage = () => {
    setExpandLanguage(!expandlanguage);
  };

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  return (
    <>
      <div
        ref={modalRef}
        className="absolute top-full right-1/6 bg-white z-10 p- w-fit flex flex-col gap-2 rounded-md shadow-md"
      >
        <div className="flex flex-col justify-center items-center gap-2 bg-main-color">
          <div className="flex justify-center items-center text-gray-200">
            <FaUserCircle size={36}></FaUserCircle>
          </div>

          <div className="flex justify-center items-center text-white">
            <span dir="rtl" className="text-gray-200">
              ثبت نام | ورود
            </span>
          </div>

          <div
            className="text-white flex justify-center items-center gap-2"
            dir="rtl"
            onClick={onLanguage}
          >
            <IoLanguageSharp></IoLanguageSharp>
            <h1>فارسی</h1>
            <IoIosArrowDown size={12}></IoIosArrowDown>

            {expandlanguage && (
              <LanguageModal onClose={onClose}></LanguageModal>
            )}
          </div>
        </div>

        <div className="flex flex-col justify-center items-start" dir="rtl">
          <div className="w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3  hover:bg-modal-hover transition delay-100 cursor-pointer">
            <FaHouse size={18}></FaHouse>
            <h1 className="text-right">صفحه اصلی</h1>
          </div>

          <div className="w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3   hover:bg-modal-hover transition delay-100 cursor-pointer">
            <ImTicket size={18}></ImTicket>
            <h1 className="text-right">پیگیری بیلط</h1>
          </div>

          <div className="w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3   hover:bg-modal-hover transition delay-100 cursor-pointer">
            <ImTicket size={18}></ImTicket>
            <h1 className="text-right">هتل داخلی</h1>
          </div>

          <div className="flex gap-2 justify-center items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3  hover:bg-modal-hover transition delay-100 cursor-pointer">
            <GrCatalog size={18}></GrCatalog>
            <h1 className="text-right"> آموزش رزرو بلیط هواپیما</h1>
          </div>

          <div className="w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3 border  hover:bg-modal-hover transition delay-100 cursor-pointer">
            <FaInfo size={18}></FaInfo>
            <h1 className="text-right">درباره ما</h1>
          </div>

          <div
            className=" w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap shadow-sm rounded-md p-3 border border-gray-200 hover:bg-modal-hover transition delay-100 cursor-pointer"
            onClick={onExpandRules}
          >
            <GiInjustice size={18}></GiInjustice>
            <h1 className="text-right">قوانین</h1>
          </div>

          <div
            className={`w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap rounded-md p-3 border  hover:bg-modal-hover transition delay-100 cursor-pointer‍‍‍ ${
              expandRules ? "visible" : "hidden"
            }`}
          >
            <h1 className="text-right text-sm text-blue-500 hover:text-black">
              قوانین بلیط چارتر
            </h1>
          </div>

          <div
            className={`w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap rounded-md p-3 border  hover:bg-modal-hover transition delay-100 cursor-pointer‍‍‍ ${
              expandRules ? "visible" : "hidden"
            }`}
          >
            <h1 className="text-right text-sm text-blue-500 hover:text-black">
              جریمه استرداد بلیط سیستمی
            </h1>
          </div>

          <div
            className={`w-full flex gap-2 justify-start items-center text-gray-600 whitespace-nowrap rounded-md p-3 border  hover:bg-modal-hover transition delay-100 cursor-pointer‍‍‍ ${
              expandRules ? "visible" : "hidden"
            }`}
          >
            <h1 className="text-right text-sm text-blue-500 hover:text-black">
              بار همراه سفر
            </h1>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserModal;
