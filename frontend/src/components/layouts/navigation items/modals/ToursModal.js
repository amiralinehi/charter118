import { BiSolidPlaneAlt } from "react-icons/bi";
import { useEffect, useRef } from "react";
const ToursModal = ({ onClose }) => {
  const modalRef = useRef();
  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  return (
    <>
      <div
        ref={modalRef}
        className="w-full  rounded-md py-2 px-4 bg-white absolute flex flex-col justify-center items-center gap-2 top-10 left-1/2 z-10"
        dir="rtl"
      >
        <div
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
          title="مسکو"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>مسکو</div>
        </div>
        <div
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
          title="تور ویژه استانبول"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>تور ویژه استانبول</div>
        </div>
        <div
          title="تور پوکت"
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>تور پوکت</div>
        </div>
        <div
          title="تور دبی"
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>تور دبی</div>
        </div>
        <div
          title="قشم"
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>قشم</div>
        </div>
      </div>
    </>
  );
};

export default ToursModal;
