import { BiSolidPlaneAlt } from "react-icons/bi";
import { useEffect, useRef } from "react";
const OtherModal = ({ onClose }) => {
  const modalRef = useRef();
  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (modalRef.current && !modalRef.current.contains(event.target)) {
        onClose();
      }
    };

    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  }, [onClose]);

  return (
    <>
      <div
        ref={modalRef}
        className="w-full  rounded-md py-2 px-4 bg-white absolute flex flex-col justify-center items-center gap-2 top-10 lef-1/2 z-10"
        dir="rtl"
      >
        <div
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
          title="کافه بازار اندروید"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>کافه بازار اندروید</div>
        </div>
        <div
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
          title="سیب چه ios"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>سیب چه ios</div>
        </div>
        <div
          title="نمایندگی فروش همکار"
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>نمایندگی فروش همکار</div>
        </div>
        <div
          title="تعاون معنا"
          className="flex justify-start items-center gap-1 w-full whitespace-nowrap overflow-hidden cursor-pointer hover:bg-modal-hover text-gray-500 text-sm font-medium"
        >
          <div>
            <BiSolidPlaneAlt />
          </div>
          <div>تعاون معنا</div>
        </div>
      </div>
    </>
  );
};

export default OtherModal;
