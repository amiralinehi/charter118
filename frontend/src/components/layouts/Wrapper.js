import Navigation from "./Navigation";
import Header from "../layouts/header/Header";
const Wrapper = (props) => {
  return (
    <div>
      <Navigation></Navigation>
      <Header></Header>
      <div>{props.children}</div>
    </div>
  );
};

export default Wrapper;
