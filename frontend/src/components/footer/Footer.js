import { SiAparat } from "react-icons/si";
import { FaFacebookF } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaTelegramPlane } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaYoutube } from "react-icons/fa";
import { SiFloatplane } from "react-icons/si";
import { MdAirplaneTicket } from "react-icons/md";
import { FaPersonWalkingLuggage } from "react-icons/fa6";

import { FaHeadset } from "react-icons/fa";
import { FaLocationDot } from "react-icons/fa6";
import { RiMoneyDollarCircleFill } from "react-icons/ri";
import { MdEmail } from "react-icons/md";
import { FaMobileScreen } from "react-icons/fa6";
import { FaCircle } from "react-icons/fa";

import Logo from "../../media/footer/footer-logo.png";
import Sibche from "../../media/footer/sibche.png";
import Bazar from "../../media/footer/bazar.png";
import Play from "../../media/footer/play.png";

import Etemad from "../../media/footer/etemad.png";
import Samandehi from "../../media/footer/samandehi.png";

const Footer = () => {
  return (
    <>
      <footer className="bg-main-color-darker w-full m-auto h-full py-2 px-6">
        <section className="py-4 h-32 flex justify-center border-b-2 border-black">
          <div className="flex justify-around items-center text-white gap-4">
            <div className="flex justify-center items-center gap-4">
              <div>
                <img alt="logo" src={Play} />
              </div>
              <div>
                <img alt="logo" src={Bazar} />
              </div>
              <div>
                <img alt="logo" src={Sibche} className="w-1/5" />
              </div>
            </div>
          </div>

          <div className="flex justify-around items-center text-white gap-4">
            <div className="flex gap-4">
              <FaYoutube size={24}></FaYoutube>
              <FaTwitter size={24}></FaTwitter>
              <FaFacebookF size={24}></FaFacebookF>
              <SiAparat size={24}></SiAparat>
              <FaInstagram size={24}></FaInstagram>
              <FaTelegramPlane size={24}></FaTelegramPlane>
            </div>
            <div>
              <img alt="logo" src={Logo} />
            </div>
          </div>
        </section>

        {/* second part or right side parent */}

        <section className="py-6 h-auto w-full flex justify-center">
          <div className="flex flex-col justify-center w-full h-full items-start gap-2">
            <div
              className="flex justify-center items-center text-center w-1/2 text-xs text-white font-semibold"
              dir="rtl"
            >
              مجوزها و نمادها
            </div>

            <div className="flex justify-center items-center text-white gap-4 ml-1">
              <div className="flex justify-center items-center gap-4">
                <div className="flex justify-center w-16 h-16 items-center bg-white bg-opacity-50 rounded-sm p-1 border-2 ">
                  <FaPersonWalkingLuggage size={44} />
                </div>

                <div className="flex justify-center w-16 h-16 items-center bg-white bg-opacity-50 rounded-sm p-1 border-2 ">
                  <SiFloatplane size={44} />
                </div>

                <div className="flex justify-center w-16 h-16 items-center bg-white bg-opacity-50 rounded-sm p-1 border-2 ">
                  <MdAirplaneTicket size={44} />
                </div>

                <div className="flex justify-center w-16 h-16 items-center bg-white bg-opacity-50 rounded-sm p-1 border-2 ">
                  <img alt="logo" src={Samandehi} />
                </div>

                <div className="flex justify-center w-16 h-16 items-center bg-white bg-opacity-50 rounded-sm p-1 border-2 ">
                  <img alt="logo" src={Etemad} />
                </div>
              </div>
            </div>

            <div
              className="flex justify-center items-center text-center w-1/2 rounded-md text-white"
              dir="rtl"
              // make background  kinda transparent
              style={{ background: "rgba(10,10,10,.2)" }}
            >
              ما دنیا را نزدیکتر میکنیم تمام بلیط‌های این وب‌سایت توسط آژانس
              مقتدر سیر ایرانیان صادر می‌شود و کلیه‌ی مسئولیت‌های صدور بلیط
              برعهده‌ی این آژانس است.
            </div>

            <div
              className="flex justify-center items-center text-center w-1/2 text-xs text-white"
              dir="rtl"
            >
              کلیه حقوق این سایت محفوظ و متعلق به چارتر118 می باشد.
            </div>
          </div>

          <div className="flex justify-around items-center text-white gap-4 text-sm">
            <div className="flex flex-col justify-center items-center">
              <div className="flex justify-center items-center gap-1 whitespace-nowra w-full">
                <div className="text-semibold">دسترسی سریع</div>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowra w-full hover:text-button-header-menu-color cursor-pointer">
                <div>هتل داخلی</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowra w-full hover:text-button-header-menu-color cursor-pointer">
                <div>پیگیری بلیط</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowra w-full hover:text-button-header-menu-color cursor-pointer">
                <div>آموزش رزرو بلیط هواپیما</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>راهنمای پرداخت</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>درباره ما</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>قوانین بلیط چارتر</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>جریمه استرداد بلیط سیستمی</div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full">
                <div>
                  کیش : بازار سارینا 1طبقه 2 واحد209 موقعیت ما بر روی نقشه
                </div>
                <FaCircle
                  size={6}
                  className="text-button-header-menu-color"
                ></FaCircle>
              </div>
            </div>

            <div className="flex flex-col justify-center items-center">
              <div className="flex justify-center items-center gap-1 whitespace-nowrap w-full mb-11">
                <div className="text-semibold ">ارتباط با ما</div>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>07691006118 </div>
                <FaHeadset className="text-button-header-menu-color"></FaHeadset>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>09027006118</div>
                <FaMobileScreen className="text-button-header-menu-color"></FaMobileScreen>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>charter118@gmail.com</div>
                <MdEmail className="text-button-header-menu-color"></MdEmail>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>5054-1670-0019-3467 </div>
                <RiMoneyDollarCircleFill className="text-button-header-menu-color"></RiMoneyDollarCircleFill>
              </div>

              <div className="flex justify-end items-center gap-1 whitespace-nowrap w-full hover:text-button-header-menu-color cursor-pointer">
                <div>
                  کیش : بازار سارینا 1طبقه 2 واحد209 موقعیت ما بر روی نقشه
                </div>
                <FaLocationDot className="text-button-header-menu-color"></FaLocationDot>
              </div>
            </div>
          </div>
        </section>
      </footer>
    </>
  );
};

export default Footer;
