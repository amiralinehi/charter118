import { useState } from "react";
import HelpPic from "../../media/help/help.jpg";

const Help = () => {
  // boolean that controls wether should content be hidden or not
  const [isExpanded, setIsExpanded] = useState(false);

  return (
    <>
      <section className="container px-8 bg-white w-3/4 m-auto flex  flex-col items-center justify-center gap-8 shadow-lg mb-1">
        {/* first paragraph */}
        <div className="flex flex-col items-end gap-3">
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            بلیط چارتر هواپیما
          </h1>
          <div className="text-gray-500 text-md" dir="rtl">
            زمانی که مسافران قصد خرید
            <span className="text-button-header-menu-color"> بلیط چارتر </span>
            هواپیما را دارند، ملاک‌هایی را برای آن در نظر می‌گیرند. اما نکته‌ای
            که شاید کمتر بدان توجه کنند، چارتری یا سیستمی بودن بلیط هواپیما است.
            فرق این دو نوع بلیط در مقصدهای خارجی بیشتر نمایان می‌شود و مسافران
            باید قبل از خرید حتما به آن توجه کنند.
          </div>
        </div>
        {/*image*/}
        <div className="flex justify-center mt-2 flex-col items-center relative">
          <img src={HelpPic} alt="charter118" className="w-1/2 h-1/2" />

          <div
            className={`bg-white w-full h-64 absolute -bottom-0 ${
              isExpanded ? "hidden" : "visible"
            }`}
          ></div>
        </div>
        {/* button */}
        {/* i used a div to caver half of image when certain condition satisfied it shows the other half picture  */}
        {!isExpanded && (
          <div className="flex flex-col text-gray-500 justify-center items-center bg-white border border-gray-500 w-fit px-6 py-1 rounded-md shadow-md bottom-o absolute">
            <button
              onClick={() => {
                setIsExpanded(!isExpanded);
              }}
            >
              بیشتر
            </button>
          </div>
        )}
        {/* eniter content block is below */}
        {/* second paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            بلیط چارتر
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            بلیط چارتر به این روش فروخته می‌شود که یک آژانس مسافرتی اقدام به
            اجاره تمام صندلی‌های یک هواپیما در یک مسیر مشخص می‌کند. سپس متناسب
            با نرخ مصوب سازمان هواپیمایی کشوری روی این بلیط‌ها قیمت‌گذاری
            می‌کند. چارترکننده‌ها مجاز به قیمت‌گذاری مورد نظر خود روی بلیط
            چارتری هستند. قیمت بلیط چارتر دارای نوسانات بسیاری است. بدین شکل که
            در زمان اوج مسافر، شاهد افزایش قیمت در بلیط‌های چارتری هستیم. اما در
            زمانی که از شدت مسافران بنابر دلایلی کاسته می‌شود؛ چارترکننده برای
            آنکه صندلی پیش خرید کرده را به فروش برساند؛ قیمت بلیط چارتری را کاهش
            می‌دهد.
          </h2>
        </div>
        {/* third paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1
            className=" text-button-header-menu-color text-2xl font-semibold"
            dir="rtl"
          >
            اجاره صندلی چارتر هواپیما
          </h1>
          <h2 className="text-gray-500 mt-2 mb-2" dir="rtl">
            «سیت چارتر» اصطلاح دیگری است که در خصوص شرایط چارتری وجود دارد.
            زمانی که یک آژانس هواپیمایی اقدام به اجاره برخی از صندلی‌های پرواز
            سیستمی از یک شرکت هواپیمایی کند؛ گفته می‌شود سیت چارتر انجام داده
            است. در این وضعیت، چارتر کننده فقط می‌تواند روی صندلی‌های اجاره شده
            قیمت‌گذاری کند و اجازه‌ای مبنی بر تعیین و تاریخ پرواز ندارد.
            <h2 className="text-gray-500" dir="rtl">
              سیت چارتر به سه صورت انجام می‌شود:
            </h2>
            <ul className=" list-disc flex flex-col gap-4 mt-4">
              <li>
                <p>اجاره شناور صندلی</p>
              </li>
              <li>
                <p>اجاره صندلی رفت‌ و برگشت</p>
              </li>
              <li>
                <p>اجاره صندلی‌های مشخص شده</p>
              </li>
            </ul>
          </h2>
        </div>
        {/* forth paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            خرید بلیط چارتر در مسیرهای پرطرفدار
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            ارزانی نسبی خرید بلیط چارتر باعث شده تا مسافران برای خرید بلیط
            هواپیما خارجی به‌سمت بلیط چارتری خارجی جذب شوند. برخی از مسیرهای
            پرواز به‌علت حجم بالای مسافر در زمان‌های گوناگون دارای بالاترین حجم
            بلیط چارتر خارجی هستند. به‌طور کل چارترکنندگان در مسیرهای پرطرفدار
            داخلی و خارجی بیشتر فعالیت دارند
          </h2>
          <h2>
            <div className="text-gray-500" dir="rtl">
              از جمله مسیرهای پر ترافیک چارتری می‌توان به
              <span dir="rtl" className="text-blue-800 font-semibold">
                بلیط هواپیما مشهد تهران، بلیط هواپیما تهران تبریز، بلیط هواپیما
                مشهد کرمان
              </span>
              بلیط هواپیما مشهد و موارد دیگری اشاره کرد.
            </div>
          </h2>
        </div>
        {/* fifth paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            چگونه بلیط چارتر بخریم؟
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            باری خرید بلیط چارتر شما می‌توانید به ابتدا همین صفحه مراجعه کرده و
            مبدا و مقصد و تاریخ پرواز خود را مشخص کنید. بعد از جستجو لیستی از
            تمامی پروازهای موجود بین مبدا و مقصد شما به نمایش در می‌آید. در این
            قسمت آن دسته از پروازهایی که برچسب اکونومی دارند را می‌توانید انتخاب
            کنید و نسبت به رزرو آنها اقدام کنید.
          </h2>
        </div>
        {/* sixth paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className=" text-blue-400 text-2xl font-semibold" dir="rtl">
            تفاوت پرواز چارتری و سیستمی در بلیط هواپیما
          </h1>
          <h2 className="text-gray-500 mt-2 mb-2" dir="rtl">
            عمده‌ترین تفاوت پرواز چارتری و سیستمی در نحوه قیمت‌گذاری و فروش
            <span className="text-blue-800 font-semibold"> بلیط هواپیما </span>
            است. همان طور که در بالاتر هم اشاره کردیم فروش بلیط چارتر بر عهده
            آژانس چارترکننده است و بلیط سیستمی را ایرلاین‌ها و آژانس‌های
            هواپیمایی به فروش می‌رسانند. اما جز بحث قیمتی، این دو بلیط در مواردی
            دیگر نیز دارای تفاوت‌هایی هستند که باید حتماً هنگام خرید بلیط
            هواپیما، بدان ها دقت کرد زیرا در برنامه سفری مسافران ممکن است
            تأثیرگذار باشد.
          </h2>
        </div>
        {/* seventh paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className=" text-blue-400 text-2xl font-semibold" dir="rtl">
            نکات مثبت و منفی بلیط چارتری
          </h1>
          <h2 className=" mt-2 mb-2 text-gray-500" dir="rtl">
            مسافران برای خرید بلیط هواپیما، قاعدتاً از بلیط‌ سیستمی یا چارتری
            استفاده می‌کنند. در ادامه به نکات مثبت و منفی بلیط چارتری می‌پردازیم
            تا هنگام خرید یا در پرواز دچار مشکل نشوید.
            <h2 className="text-gray-500" dir="rtl">
              سیت چارتر به سه صورت انجام می‌شود:
            </h2>
            <ul
              className=" list-disc flex flex-col gap-4 mt-4 text-gray-500"
              dir="rtl"
            >
              <li>
                <p dir="rtl">
                  بسیاری بر این باورند که خرید بلیط چارتر، آن‌ها را به
                  <span className="text-blue-800 font-semibold">
                    {" "}
                    بلیط ارزان هواپیما{" "}
                  </span>
                  نزدیک می‌کند. اما این کاملاً باور غلطی است. چارترکننده
                  به‌دنبال سود خود است پس لزومی به ارزان فروشی ندارد. اگر
                  به‌دنبال خرید بلیط ارزان هواپیما هستید، پس باید زمان‌های کم
                  سفر یا ایام کاهش مسافر را انتخاب کنید
                </p>
              </li>
              <li>
                <p dir="rtl">
                  یکی از نکات منفی که بسیاری از مسافران به پروازهای چارتر نسبت
                  می‌دهند؛ تأخیر در زمان پرواز است. در بلیط‌های چارتری ارسال
                  مشخصات خریدار، دست چارتر کننده است که باید به شرکت هواپیمایی
                  مذکور ارسال کند که گاهی اوقات این اتفاق می‌افتد که اسامی برخی
                  از مسافران در برنامه پروازی ثبت نمی‌شود. همین پروسه سبب تأخیر
                  پرواز یا جاماندن از پرواز می‌شود. البته اگر
                  <span className="text-blue-800 font-semibold">
                    {" "}
                    بلیط لحظه آخری{" "}
                  </span>
                  هواپیما خود را از چارتر 118 رزرو کنید، هیچگاه به این مشکل بر
                  نمی‌خورید.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  بلیط چارتری اکثراً برای مسیرهای خاص و پرطرفدار فروخته می‌شود.
                </p>
              </li>
            </ul>
          </h2>
        </div>
        {/* eighth paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className=" text-blue-400 text-2xl font-semibold" dir="rtl">
            نکات مثبت و منفی بلیط چارتری{" "}
          </h1>
          <h2 className="text-gray-500 mt-2 mb-2" dir="rtl">
            اما خرید بلیط چارتر در چه زمان‌هایی توصیه نمی‌شود که در ادامه به شرح
            آنها خواهیم پرداخت:
            <ul className=" list-disc flex flex-col gap-4 mt-4" dir="rtl">
              <li>
                <p dir="rtl">
                  اولین مورد زمانی است که با خود کودک همراه دارید. قیمت بلیط
                  کودکان در نوع چارتری با بزرگسالان یکسان است به همین دلیل اگر
                  با خود کودک همراه دارید بهتر است که از این نوع بلیط استفاده
                  نکنید.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  مورد دوم زمانی است که در ایام پر سفر سال قرار داریم. در این
                  روزها گاها قیمت بلیط چارتری از سیستمی نیز بیشتر می‌شود به همین
                  دلیل بهتر است از خرید بلیط در این ایام بپرهیزید.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  همچنین اگر از سفر خود اطمینان ندارید بلیط هواپیما چارتر
                  خریداری نکنید چرا که معمولا درصد جریمه کنسلی این نوع بلیط
                  بسیار بالا می‌باشد.
                </p>
              </li>
            </ul>
          </h2>
        </div>
        {/* ninth  paragrapgh*/}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            آیا امکان کنسلی بلیط چارتر هواپیما وجود دارد؟
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            در مورد کنسلی بلیط چارتر هواپیما باید گفت که این نوع بلیط را سایت‌ها
            و آژانس‌های مختلف هواپیمایی کنسل نمی‌کنند اما باید در این مورد گفت
            که چارتر 118 این نوع بلیط را نیز مطابق با قوانین کنسل می‌کند و وجه
            پرداختی شما را نیز برگشت خواهد زد. همچنین کسانی که قصد
            <span className="text-blue-800 font-semibold">
              {" "}
              خرید فوری بلیط هواپیما{" "}
            </span>
            را دارند چه از نوع چارتری یا سیستمی می‌توانند در سایت ما نیز اقدام
            به رزرو بلیط مد نظر خود کنند.
          </h2>
        </div>
        {/* tenth paragrath */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            پرواز چارتر
          </h1>
          <h2 className="text-blue-400" dir="rtl">
            در پرواز چارتر قیمت بلیط کودکان با بزرگسالان یکسان است. بنابراین اگر
            قصد سفر خانوادگی دارید باید قیمت برابری برای صندلی بزرگسالان و
            کودکان پرداخت کنید. در مورد پرواز چارتری نیز باید به این نکته توجه
            داشته باشید که هر چه به تایم پرواز نزدیکتر باشید می‌توانید بلیط را
            با قیمت کمتری رزرو کنید. همچنین از پر طرفدارترین پروازهای چارتری
            می‌توان به
            <span className="text-blue-800 font-semibold">
              {" "}
              بلیط هواپیما تهران استانبول، بلیط هواپیما تهران مشهد و بلیط
              هواپیما تبریز تهران{" "}
            </span>
            اشاره کرد.
          </h2>
        </div>
        {/* eleventh paragrath */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            بلیط چارتر خارجی
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            اگر قصد خرید بلیط چارتر خارجی را نیز دارید ما شرایطی را فراهم کردیم
            که تمامی پروازهای مهم خارج از ایران را نیز پوشش دهیم. همچنین اگر
            مقصد خارجی خاصی مد نظر داشتید که در لیست پروازهای ما نبود نگران
            نباشید. ما برای کسانی که عاشق سفرهای خارج از ایران هستند وبسایتی
            اختصاصی به نام bookcharter118 آماده کردیم که در آن به هر شهر و کشوری
            که قصد سفر داشتید بتوانید بلیط آن را با کمترین قیمت رزرو کرده و از
            سفر خود لذت ببرید
          </h2>
        </div>
        {/* twelfth paragraph */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            میزان بار مجاز برای بلیط چارتر هواپیما
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            سقف میزان بار مجاز در هر ایرلاین متفاوت است و هریک میزان دلخواهی را
            تعیین کرده‌اند به همین دلیل چارتری بودن آن بلیط تاثیر خاصی در این
            موضوع ندارد. از عوامل تعیین کننده در سقف بار مجاز می‌توان به
            کلاس‌های پروازی اشاره کرد که حمل بار مسافران را معین می‌کند. به‌طور
            مثال در ایرلاین‌های داخلی میزان بار مجاز در بلیط هواپیما 20 کیلوگرم
            است.
          </h2>
        </div>
        {/* thirtinth */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className="text-blue-400 text-2xl font-semibold" dir="rtl">
            بهترین مسیرها برای خرید بلیط چارتر
          </h1>
          <h2 className="text-gray-500" dir="rtl">
            ارزان بودن بلیط چارتر در زمان‌هایی که سفرهای کمی در جریان است یا
            دیگر عوامل موثری که قبلا گفته شده باعث می‌شود تا مسافران اقبال بسیار
            زیادی نسبت به این نوع بلیط داشته باشند. از مهمترین مسیرها برای بلیط
            چارتری می‌توان به بلیط چارتر مشهد، بلیط چارتر استانبول و همچنین بلیط
            چارتر دبی اشاره کرد. پروازهای خارجی به علت قیمت بالایی که دارند باعث
            شده ک مسافران روی به خرید بلیط چارتری برای این مقاصد داشته باشند. در
            چارتر 118 شما می‌توانید به راحتی انواع بلیط چارتر داخلی و خارجی را
            مشاهده کنید و نسبت به رزرو آن اقدام کنید.
          </h2>
        </div>
        {/* fourteenth paragrath */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className=" text-blue-400 text-2xl font-semibold" dir="rtl">
            مزایای چارتر 118 برای خرید بلیط چارتر هواپیما
          </h1>
          <h2 className="text-gray-500 mt-2 mb-2" dir="rtl">
            <ul className=" list-disc flex flex-col gap-4 mt-4" dir="rtl">
              <li>
                <p dir="rtl">
                  هنگام مراجعه به سایت چارتر 118 و وارد کردن مقصد و زمان سفر؛
                  لیست کاملی از تمام پروازهای سیستمی و چارتری در اختیار مسافران
                  قرار می‌گیرد.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  اطلاعات کاملی از ساعت پرواز، نوع پرواز، کلاس پروازی و قیمت
                  بلیط قابل‌مشاهده است.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  تمامی بلیط‌های سایت چارتر 118 بی‌واسطه و بدون افزایش نرخ به
                  فروش می‌رسند. که مسافران می‌توانند از ارزان بودن بلیط‌های این
                  سایت اطمینان حاصل کنند.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  پشتیبانی 24 ساعته، مسافران را در امر خرید بلیط از ابتدا تا
                  انتها یاری می‌رسانند.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  امکان استفاده از تخفیف‌های آنلاین از نکات برجسته دیگر این سایت
                  است.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  امکان مقایسه هم‌زمان پروازهای مختلف در چارتر 118 وجود دارد.
                </p>
              </li>
              <li>
                <p dir="rtl">
                  اولین وب سایت فروش پرواز سیستمی و چارتری در ایران فعالیت رسمی
                  از سال 1392
                </p>
              </li>
            </ul>
          </h2>
        </div>
        {/* fifteenth */}
        <div
          className={`flex flex-col items-end gap-3 ${
            isExpanded ? "visible" : "hidden"
          }`}
        >
          <h1 className=" text-blue-400 text-2xl font-semibold" dir="rtl">
            سوالات متداول در مورد بلیط چارتر
          </h1>
          <h2 className="text-gray-500 mt-2 mb-2" dir="rtl">
            <ul className=" list-disc flex flex-col gap-4 mt-4" dir="rtl">
              <li>
                <p dir="rtl">بلیط چارتر ارزانتر است یا سیستمی؟</p>
                <p dir="rtl">
                  باید گفت که با توجه به شرایط زمانی که اقدام به رزرو بلیط چارتر
                  می‌کنید قیمت آن نیز متغیر است ولی قیمت بلیط سیستمی همیشه ثابت
                  است.
                </p>
              </li>
              <li>
                <p dir="rtl">آیا قیمت بلیط چارتر برای کودکان پایین‌تر است؟</p>
                <p dir="rtl">
                  خیر قیمت بلیط چارتری برای کودکان همانند بزرگسالان می‌باشد.
                </p>
              </li>
              <li>
                <p dir="rtl">بلیط چارتر همان بلیط لحظه آخری است؟</p>
                <p dir="rtl">
                  بله بلیط لحظه آخری نوعی بلیط چارتر محسوب می‌شود.{" "}
                </p>
              </li>
              <li>
                <p dir="rtl">آیا بلیط چارتر را می‌توان پس داد؟</p>
                <p dir="rtl">
                  استرداد بلیط چارتر معمولا به سختی امکان پذیر است بنابراین
                  زمانی که از سفر خود اطمینان دارید نسبت به رزرو این نوع بلیط
                  اقدام کنید.
                </p>
              </li>
              <li>
                <p dir="rtl">بهترین زمان خرید بلیط چارتر چه وقتی است؟</p>
                <p dir="rtl">
                  در شرایط گوناگونی شما می‌توانید نسبت به خرید بلیط چارتر اقدام
                  کنید از جمله مهمترین آنها زمانی است تعطیلات خاصی پیش رو نیست و
                  سفرهای کمی نیز در جریان است.
                </p>
              </li>
              <li>
                <p dir="rtl">در پرواز چارتر میزان بار مجاز چقدر است؟</p>
                <p dir="rtl">
                  میزان بار مجاز برای پرواز چارتر و دیگر پروازها تفاوتی ندارد.
                  معیاری که بر این امر اثر گذار است کلاس پروازی و همچنین کلاس
                  نرخی پرواز است. به طور میانگین نیز میزان بار مجاز 20 کیلو گرم
                  می‌باشد.
                </p>
              </li>
            </ul>
          </h2>
        </div>

        {/* button */}
        {isExpanded && (
          <div className="flex flex-col text-gray-500 justify-center items-center bg-white border border-gray-500 w-fit px-6 py-1 rounded-md shadow-md">
            <button
              onClick={() => {
                setIsExpanded(!isExpanded);
              }}
            >
              کمتر
            </button>
          </div>
        )}
      </section>
    </>
  );
};

export default Help;
