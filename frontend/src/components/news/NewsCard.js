import { IoIosArrowBack } from "react-icons/io";

// func to convert eng digits to per digits
const toPersianDigits = (number) => {
  const persianDigits = "۰۱۲۳۴۵۶۷۸۹";
  return number.toString().replace(/\d/g, (digit) => persianDigits[digit]);
};

const NewsCard = () => {
  const randomPersianNumber = toPersianDigits(Math.round(Math.random() * 100));
  let title = `راهنمای سفر چارتر ۱۱۸ ${randomPersianNumber}`;

  return (
    <>
      <div className="flex justify-center items-center bg-white w-fit ">
        <div className="flex flex-col justify-center items-center w-fit">
          <div
            style={{ borderBottom: "2px solid #A02777" }}
            className="text-gray-500 font-semibold"
            dir="rtl"
          >
            {title}
          </div>
          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>

          <div
            className="text-sm font-medium flex justify-start items-center w-full"
            dir="rtl"
          >
            <IoIosArrowBack className="text-blue-400"></IoIosArrowBack>
            <div
              dir="rtl"
              className="text-center text-gray-500 overflow-hidden overflow-ellipsis hover:text-black"
            >
              مراکز دیدنی قم
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NewsCard;
