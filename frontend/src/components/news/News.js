import { useState } from "react";
import NewsCard from "../news/NewsCard";

const News = () => {
  const [visibleCardCount, setVisibleCardCount] = useState(4); // initial card numbers we can see
  const totalCards = 16; // total number of cards

  const handleMoreButtonClick = () => {
    // show all cards when the "بیشتر" button is clicked
    setVisibleCardCount(totalCards);
  };

  const handleLessButtonClick = () => {
    // show only 4 cards when the "کمتر" button is clicked
    setVisibleCardCount(4);
  };

  return (
    <>
      <section
        className="bg-white gap-6 flex flex-wrap justify-center items-center w-full p-6 border-t-2 border-black"
        dir="rtl"
        id="news-section"
      >
        {/* show the initially visible cards based on the visibleCardCount */}
        {[...Array(visibleCardCount)].map((_, index) => (
          <NewsCard key={index} />
        ))}

        {/* "More" button to toggle visibility of additional cards */}
        {visibleCardCount < totalCards && (
          <button
            onClick={handleMoreButtonClick}
            className="bg-white border-2 rounded-md px-6 py-1 border-gray-300 text-gray-500"
            dir="rtl"
          >
            بیشتر...
          </button>
        )}

        {/* "Less" button to toggle visibility of fewer cards */}
        {visibleCardCount > 4 && (
          <button
            onClick={handleLessButtonClick}
            className="bg-white border-2 rounded-md px-6 py-1 border-gray-300 text-gray-500"
          >
            کمتر
          </button>
        )}
      </section>
    </>
  );
};

export default News;
