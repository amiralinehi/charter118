import Kish from "../../media/ticket/kish.jpg";

const cities = [
  "کیش",
  "اصفهان",
  "مشهد",
  "تهران",
  "اهواز",
  "بندر عباس",
  "تبریز",
  "شیراز",
];

const Card = (props) => {
  const filtered = cities.filter((city) => {
    return city !== props.from;
  });
  return (
    <div className="flex flex-col bg-white rounded-md shadow-md p-1 w-1/5">
      <div className="bg-gradient-to-r from-white to-main-color to-70% bg-cover bg-center bg-no-repeat relative">
        <img
          src={props.image}
          alt="city-pic"
          className="w-full h-full mix-blend-overlay"
        />
        <div className="absolute top-3 right-0 flex flex-col py-1 pr-1">
          <h2 className="text-xs text-white font-semibold mb-1" dir="rtl">
            بلیط هواپیما
          </h2>
          <div
            dir="rtl"
            className="text-white relative rounded-md mb-1 w-fit h-auto"
            style={{ background: "rgba(0, 0, 0, 0.5)" }}
          >
            <h1 className="text-white relative z-10">{props.from} به</h1>
          </div>
        </div>
      </div>
      <div className="flex flex-col justify-between items-center w-full mt-1">
        <div
          className="overflow-y-scroll w-full h-56 flex flex-col text-sm font-semibold"
          dir="rtl"
        >
          {filtered.map((city, index) => {
            return (
              <div
                key={index}
                className="flex justify-around items-center border-b border-gray-300 hover:bg-card-hover transition delay-100  cursor-pointer"
              >
                <div className="py-1 px-3 m-1 rounded-md">{city}</div>
                <div className="flex justify-center items-center gap-1">
                  <span>{props.price}</span>
                  <span className="text-xs">تومان</span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Card;
