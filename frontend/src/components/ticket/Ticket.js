import Card from "./Card";
import Kish from "../../media/ticket/kish.jpg";
import Isfahan from "../../media/ticket/isfahan.jpg";
import Tabriz from "../../media/ticket/tabriz.jpg";
import Tehran from "../../media/ticket/tehran.jpg";
import Ahvaz from "../../media/ticket/ahvaz.jpg";
import Shiraz from "../../media/ticket/shiraz.jpg";
import Bandar from "../../media/ticket/bandar.jpg";
import Mashhad from "../../media/ticket/mashahd.jpg";

const Ticket = () => {
  return (
    <>
      <section className="bg-slate-100 flex justify-center items-center p-1 max-w-screen-xl mx-auto flex-col gap-6 text-gray-500">
        <h1 className=" text-xl border-b-2 border-main-color font-semibold text-gray-500">
          بلیط چارتر
        </h1>
        <div className="flex justify-center items-center flex-wrap gap-6">
          <Card
            from="کیش"
            image={Kish}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="اصفهان"
            image={Isfahan}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="مشهد"
            image={Mashhad}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="تهران"
            image={Tehran}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="اهواز"
            image={Ahvaz}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="بندرعباس"
            image={Bandar}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="تبریز"
            image={Tabriz}
            price={Math.round(Math.random() * 1000)}
          ></Card>
          <Card
            from="شیراز"
            image={Shiraz}
            price={Math.round(Math.random() * 1000)}
          ></Card>
        </div>
        <h1 className=" text-xl border-b-2 border-main-color font-semibold text-gray-500 mb-4">
          خرید بلیط چارتر هواپیما در چارتر 118
        </h1>
      </section>
    </>
  );
};

export default Ticket;
