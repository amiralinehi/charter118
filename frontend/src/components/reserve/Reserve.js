import TopPic from "../../media/reserve/top-buy-ticket.jpg";
import LeftPic from "../../media/reserve/bottom-left-hotel-reserve.jpg";
import RightPic from "../../media/reserve/bottom-right-hotel-reserve.jpg";

const Reserve = () => {
  return (
    <section className="container">
      <div className="flex flex-col w-fit m-auto justify-center items-center gap-6">
        {/* top pic */}
        <div className="flex justify-center">
          <img
            src={TopPic}
            alt="top-pic-ticket-reserve"
            className="w-fit rounded-md cursor-pointer"
          />
        </div>
        {/* bottom pics */}
        <div className="flex items-center gap-6">
          <div>
            <img
              src={LeftPic}
              alt="left-pic-ticket-reserve"
              className="w-fit rounded-md cursor-pointer"
            />
          </div>
          <div>
            <img
              src={RightPic}
              alt="right-pic-ticket-reserve"
              className="w-fit rounded-md cursor-pointer"
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Reserve;
