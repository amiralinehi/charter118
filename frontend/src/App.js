import React, { useState, useEffect } from "react";
import Wrapper from "./components/layouts/Wrapper";
import Reserve from "./components/reserve/Reserve";
import Ticket from "./components/ticket/Ticket";
import Help from "./components/help/Help";
import About from "./components/about/About";
import News from "./components/news/News";
import Footer from "./components/footer/Footer";
import { FaWhatsapp } from "react-icons/fa";
import { IoIosArrowDropup } from "react-icons/io";
import WhatsappModal from "./components/whatsapp/WhatappModal";

function App() {
  const [scrollPosition, setScrollPosition] = useState(0);
  const [whatsApp, setWhatsApp] = useState(false);

  const onWhatsAppClick = () => [setWhatsApp(!whatsApp)];
  useEffect(() => {
    const handleScroll = () => {
      setScrollPosition(window.scrollY);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const whatsappStyle = {
    top: `${Math.min(scrollPosition, 100)}px`,
  };

  return (
    <section className="bg-slate-100">
      <div
        className="flex gap-1 sticky"
        style={whatsappStyle}
        onClick={onWhatsAppClick}
      >
        <div className="absolute right-0 top-0 bottom-0 p-2">
          <div className="bg-green-400 rounded-full">
            <FaWhatsapp className="text-white" size={44}></FaWhatsapp>
          </div>
          {whatsApp && <WhatsappModal></WhatsappModal>}
        </div>
      </div>

      <div className="flex gap-1 sticky" style={whatsappStyle}>
        <a href="#top-nav">
          <div className="absolute -bottom-1/2 p-2">
            <div className="bg-main-color rounded-full">
              <IoIosArrowDropup
                className="text-white"
                size={22}
              ></IoIosArrowDropup>
            </div>
          </div>
        </a>
      </div>
      <Wrapper></Wrapper>
      <Ticket></Ticket>
      <Help></Help>
      <Reserve></Reserve>
      <About></About>
      <News></News>
      <Footer></Footer>
    </section>
  );
}

export default App;
